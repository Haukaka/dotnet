//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebShop.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class donhang
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public donhang()
        {
            this.ctdhs = new HashSet<ctdh>();
        }
    
        public int id_donhang { get; set; }
        public Nullable<int> id_user { get; set; }
        public Nullable<int> id_khuyenmai { get; set; }
        public string diachi { get; set; }
        public Nullable<System.DateTime> ngay_tao_hd { get; set; }
        public Nullable<int> sdt { get; set; }
        public Nullable<decimal> tong_tien { get; set; }
        public Nullable<System.DateTime> ngaygiaodh { get; set; }
        public Nullable<int> idtinhtrang { get; set; }
        public Nullable<int> idthanhtoan { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctdh> ctdhs { get; set; }
        public virtual giaohang giaohang { get; set; }
        public virtual thanhtoan thanhtoan { get; set; }
        public virtual khuyenmai khuyenmai { get; set; }
        public virtual user user { get; set; }
    }
}
