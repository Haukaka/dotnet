﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using 2 thu vien thiet ke metadata
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebShop.Models
{
    [MetadataTypeAttribute(typeof(sanphamMetadata))]
    public partial class sanpham
    {
        // chi su dung cho 1 class va k cho ke thua
        internal sealed class sanphamMetadata
        {
            public int id { get; set; }
            [Display(Name = "Akin")]
            public Nullable<int> giongnhau { get; set; }
            [Display(Name = "Kind of Pro")]
            public Nullable<int> id_loaihang { get; set; }
            [Display(Name = "Classify ID")]
            public Nullable<int> id_phanloai { get; set; }
            [Display(Name = "Name")]
            public string ten_sp { get; set; }
            [Display(Name = "Price")]
            public Nullable<double> gia { get; set; }
            [Display(Name = "Sale %")]
            public Nullable<int> phantramgiamgia { get; set; }
            [Display(Name = "Sale Price")]
            public Nullable<double> gia_moi { get; set; }
            [Display(Name = "Size")]
            public string size { get; set; }
            [Display(Name = "Describe Design")]
            public string mo_ta_kieu_dang { get; set; }
            [Display(Name = "Describe Color")]
            public string mo_ta_mau_sac { get; set; }
            [Display(Name = "Image")]
            public string hinh_anh { get; set; }
            [Display(Name = "Quantum")]
            public Nullable<int> soluonng { get; set; }
           
           
        }
    }
}