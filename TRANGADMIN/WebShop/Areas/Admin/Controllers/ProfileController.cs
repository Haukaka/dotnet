﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Dao;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
	public class ProfileController : Controller
	{
		thoitrangnamEntities4 db = new thoitrangnamEntities4();
		// GET: Admin/Profile
		public ActionResult Details()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);
			if (session == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			return View(user);
		}
		//sua
		[HttpGet]
		public ActionResult Edit()
		{
			var session = (user)Session["Taikhoan"];
			user user = db.users.SingleOrDefault(n => n.id_user == session.id_user);

			if (user == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(user);
		}
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(user user)
		{
			var dao = new UserDao();

			user u = db.users.SingleOrDefault(n => n.id_user == user.id_user);
			u.ten_khach_hang = user.ten_khach_hang;
			if (u.pass.Equals(user.pass))
			{
				u.pass = user.pass;
			}
			else
			{
				u.pass = user.pass;
				u.pass = dao.ToMD5(user.pass);

			}
			u.tenuser = user.tenuser;
			u.ngay_sinh = user.ngay_sinh;
			u.mail = user.mail;
			u.sdt = user.sdt;
			u.gioi_tinh = user.gioi_tinh;
			u.dia_chi1 = user.dia_chi1;

			db.SaveChanges();
			return RedirectToAction("Details");
		}
	}
}