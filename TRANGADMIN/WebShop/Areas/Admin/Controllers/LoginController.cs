﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Areas.Admin.Models;
using WebShop.Common;
using WebShop.Dao;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
	public class LoginController : Controller
	{
		// GET: Admin/Login

		
		//1

		//2
		thoitrangnamEntities4 db = new thoitrangnamEntities4();
		[HttpGet]
		public ActionResult Login()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Login(FormCollection f)
		{
			string tentk = f["username"].ToString();
			string mk = f.Get("password").ToString();
			var dao = new UserDao();
			string pass = dao.ToMD5(mk);
			user kh = db.users.SingleOrDefault(n => n.tenuser == tentk && n.pass == pass && n.nhom=="1");
			if (kh != null)
			{
				ViewBag.ThongBao = "Chúc mừng bạn đăng nhập thành công";

				Session["Taikhoan"] = kh;
				return new RedirectResult("/Admin/QuanLyUser/Index");
			}
			ViewBag.ThongBao = "Tên tài khoản hoặc mật khẩu không đúng";
			return View();
		}
		public ActionResult Logout()
		{
			Session["Taikhoan"] = null;
			return RedirectToAction("Login", "Login");
		}
	}
}