﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;
using WebShop.Areas.Admin.Models;
using System.Net.Mail;
using System.Net;

namespace WebShop.Areas.Admin.Controllers
{
	public class PhanhoiController : Controller
	{
		thoitrangnamEntities4 db = new thoitrangnamEntities4();
		// GET: Admin/Contact
		public ActionResult Index(string searchString, int? page)
		{
			int pageSize = 5;
			int pageNumber = (page ?? 1);
			return View(db.phanhois.Where(n => n.tieude.Contains(searchString) || searchString == null).ToList().OrderByDescending(n => n.ngaygui).ToPagedList(pageNumber, pageSize));

		}

		//Chi tiet contact
		public ActionResult Details(int? id)
		{
			phanhoi ph = db.phanhois.SingleOrDefault(n => n.idphanhoi == id);
			if (ph == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			return View(ph);
		}

		//xóa contact
		[HttpGet]
		public ActionResult Delete(int? id)
		{
			phanhoi ph = db.phanhois.SingleOrDefault(n => n.idphanhoi == id);
			if (ph == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(ph);
		}

		[HttpPost, ActionName("Delete")]
		public ActionResult Delete(int id)
		{
			phanhoi ph = db.phanhois.SingleOrDefault(n => n.idphanhoi == id);
			if (ph == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			db.phanhois.Remove(ph);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		//Sua mail
		[HttpGet]
		public ActionResult Edit(int? id)
		{
			ViewBag.idstatusgui = new SelectList(db.statusphanhois.ToList().OrderBy(n => n.status), "id", "status");
			phanhoi ph = db.phanhois.SingleOrDefault(n => n.idphanhoi == id);
			if (ph == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(ph);
		}
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(phanhoi ph, FormCollection f)
		{
			if (ModelState.IsValid)
			{
				//thuc hien gui mail
				MailMessage mm = new MailMessage("cskhbrazza123@gmail.com", ph.email);
				mm.Subject = ph.tieude;
				mm.Body = ph.noidungphanhoi.ToString();
				mm.IsBodyHtml = true;

				SmtpClient smtp = new SmtpClient();
				smtp.Host = "smtp.gmail.com";
				smtp.Port = 587;
				smtp.EnableSsl = true;

				NetworkCredential nc = new NetworkCredential("cskhbrazza123@gmail.com", "cskh123456");
				smtp.UseDefaultCredentials = true;
				smtp.Credentials = nc;
				smtp.Send(mm);
				// thay doi thanh da tra loi
				ph.idstatusgui = 2;

				//Thuc hien cap nhat trong model
				db.Entry(ph).State = System.Data.Entity.EntityState.Modified;
				db.SaveChanges();
			}
			return RedirectToAction("Index");
		}
	}
}
