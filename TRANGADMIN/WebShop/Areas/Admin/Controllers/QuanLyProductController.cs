﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
    public class QuanLyProductController : Controller
    {
        thoitrangnamEntities4 db = new thoitrangnamEntities4();
        // GET: Admin/QuanLyProduct
        public ActionResult Index(string searchString,int? page)
        {
            // int pageSize = 5;
            // int pageNumber = (page ?? 1);
            //return View(db.sanphams.ToList().OrderBy(n => n.id).ToPagedList(pageNumber, pageSize));
            List<user> lstKq = db.users.ToList();
            return View(db.sanphams.Where(n => n.ten_sp.Contains(searchString) || searchString == null).ToList().ToPagedList(page ?? 1, 9));
        }

        //THEM MOI
        [HttpGet]
        public ActionResult Create()
        {
            //đưa dữ liệu vào dropdown list ten_chatlieu	ten_loaihang	ten_mau	ten_phanloai
            ViewBag.id_loaihang = new SelectList(db.loaihangs.ToList().OrderBy(n => n.ten_loaihang), "id_loaihang", "ten_loaihang");
            ViewBag.id_phanloai = new SelectList(db.phanloais.ToList().OrderBy(n => n.ten_phanloai), "id_phanloai", "ten_phanloai");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(sanpham sp)
        {
            
            db.sanphams.Add(sp);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //Chi tiet sp
        public ActionResult Details(int? id)
        {
            sanpham sp = db.sanphams.SingleOrDefault(n => n.id == id);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(sp);
        }

        //xóa sp
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            sanpham sp = db.sanphams.SingleOrDefault(n => n.id == id);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(sp);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            sanpham sp = db.sanphams.SingleOrDefault(n => n.id == id);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.sanphams.Remove(sp);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Sua sp
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            //đưa dữ liệu vào dropdown list ten_chatlieu	ten_loaihang	ten_mau	ten_phanloai
            ViewBag.id_loaihang = new SelectList(db.loaihangs.ToList().OrderBy(n => n.ten_loaihang), "id_loaihang", "ten_loaihang");
            ViewBag.id_phanloai = new SelectList(db.phanloais.ToList().OrderBy(n => n.ten_phanloai), "id_phanloai", "ten_phanloai");
            sanpham sp = db.sanphams.SingleOrDefault(n => n.id == id);
            if (sp == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(sp);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(sanpham sp, FormCollection f)
        {
            if (ModelState.IsValid)
            {
                //Thuc hien cap nhat trong model
                db.Entry(sp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}