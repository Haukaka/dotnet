﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
	public class QuanLyDonHangController : Controller
	{
		thoitrangnamEntities4 db = new thoitrangnamEntities4();
		// GET: Admin/QuanLyDonHang
		public ActionResult Index(string searchString, int? page)
		{
			int pageSize = 5;
			int pageNumber = (page ?? 1);
			return View(db.donhangs.Where(n => n.user.ten_khach_hang.Contains(searchString) || searchString == null).ToList().OrderByDescending(n => n.id_user).ToPagedList(pageNumber, pageSize));
		}
		//Sua sp
		[HttpGet]
		public ActionResult Edit(int? id)
		{
			ViewBag.idtinhtrang = new SelectList(db.giaohangs.ToList().OrderBy(n => n.status), "id", "status");
			ViewBag.idthanhtoan = new SelectList(db.thanhtoans.ToList().OrderBy(n => n.status), "id", "status");
			//đưa dữ liệu vào dropdown list ten_chatlieu	ten_loaihang	ten_mau	ten_phanloai
			donhang sp = db.donhangs.SingleOrDefault(n => n.id_donhang == id);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(sp);
		}
			
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(donhang ph, FormCollection f)
		{
			var session = (user)Session["Taikhoan"];
			if (ModelState.IsValid)
			{
				//thuc hien gui mail
				MailMessage mm = new MailMessage("cskhbrazza123@gmail.com",session.mail);
				mm.Subject = "Xác nhận đơn hàng";
				mm.Body = "Đơn hàng đã được xác nhận";
				mm.IsBodyHtml = true;

				SmtpClient smtp = new SmtpClient();
				smtp.Host = "smtp.gmail.com";
				smtp.Port = 587;
				smtp.EnableSsl = true;

				NetworkCredential nc = new NetworkCredential("cskhbrazza123@gmail.com", "cskh123456");
				smtp.UseDefaultCredentials = true;
				smtp.Credentials = nc;
				smtp.Send(mm);
				// thay doi thanh da tra loi

				donhang dh = db.donhangs.SingleOrDefault(n=>n.id_donhang==ph.id_donhang);
				dh.idtinhtrang = 1;
				dh.idthanhtoan = ph.idthanhtoan;
				db.SaveChanges();
			}
			return RedirectToAction("Index");
		}
		//xem chi tiet don hang
		public ActionResult ctdh(int? iddh)
		{
			List<ctdh> ct = db.ctdhs.Where(n => n.id_donhang == iddh).ToList();
			return View(ct);
		}
		//xóa sp
		[HttpGet]
		public ActionResult Delete(int? id)
		{
			donhang sp = db.donhangs.SingleOrDefault(n => n.id_donhang == id);
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}

			return View(sp);
		}

		[HttpPost, ActionName("Delete")]
		public ActionResult Delete(int id)
		{
			donhang sp = db.donhangs.SingleOrDefault(n => n.id_donhang == id);
			List<ctdh> ctdh = db.ctdhs.Where(n => n.id_donhang == id).ToList();
			if (sp == null)
			{
				Response.StatusCode = 404;
				return null;
			}
			foreach (var item in ctdh)
			{

				db.ctdhs.Remove(item);
			}
			db.donhangs.Remove(sp);
			db.SaveChanges();
			return RedirectToAction("Index");
		}
	}
}