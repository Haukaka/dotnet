﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Areas.Admin.Controllers
{
    public class QuanLyPhanLoaiController : Controller
    {
        thoitrangnamEntities4 db = new thoitrangnamEntities4();
        // GET: Admin/QuanLyUser
        public ActionResult Index(string searchString, int? page)
        {
            //Phan trang
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(db.phanloais.Where(n => n.ten_phanloai.Contains(searchString) || searchString == null).ToList().OrderBy(n => n.id_phanloai).ToPagedList(page ?? 1, 5));
           
        }


        //THEM MOI
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(phanloai pl)
        {
            db.phanloais.Add(pl);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //Chi tiet phan loai
        public ActionResult Details(int? id)
        {
            phanloai pl = db.phanloais.SingleOrDefault(n => n.id_phanloai == id);
            if (pl == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(pl);
        }

        //xóa phan loai
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            phanloai pl = db.phanloais.SingleOrDefault(n => n.id_phanloai == id);
            if (pl == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(pl);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            phanloai pl = db.phanloais.SingleOrDefault(n => n.id_phanloai == id);
            if (pl == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            db.phanloais.Remove(pl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Sua phan loai
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            phanloai pl = db.phanloais.SingleOrDefault(n => n.id_phanloai == id);
            if (pl == null)
            {
                Response.StatusCode = 404;
                return null;
            }

            return View(pl);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(phanloai pl, FormCollection f)
        {
            if (ModelState.IsValid)
            {
                //Thuc hien cap nhat trong model
                db.Entry(pl).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}